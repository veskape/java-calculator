/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.tse.calculator;

/**
 *
 * @author Charles Bacchis
 */

/*Classe de lancement du projet.
Le projet est séparé en 3 classes:
- main: classe de lancement du projet
- Calculator: classe d'affichage des éléments
- ButtonListener: classe de listener et gestion des évènements
- Calculations: classe regroupant les différentes fonctions de calcul

Le projet est un projet Java Maven entièrement fait sur netbeans
cela implique que certaines parties du code ont été auto-générées et étaient
"protégées" à l'écriture, ces parties autogénérées et protégées seront annotées
d'un commentaire. J'ai réussi à lever la protection afin de modifier l'appel de
la classe listener (plus d'informations au début de ButtonListener.java.*/
public class main {
        public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Calculator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Calculator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Calculator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Calculator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                //instanciation de la classe principale Calculator
                new Calculator().setVisible(true);
            }
        });
    }
}
