/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.tse.calculator;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Charles Bacchis
 */
//Classe qui répertorie les différentes formules mathématiques à utiliser
public class Calculations {
    //on récupère notre fenetre principale pour accéder aux éléments
    private Calculator fenetre;

    public Calculations(Calculator fenetre) {
        super();
        this.fenetre = fenetre;
    }

    //méthodes de calcul mathématique
    public void add(double x, double y) {
        setResult(x + y);
    }

    public void substract(double x, double y) {
        setResult(x - y);
    }

    public void multiply(double x, double y) {
        setResult(x * y);
    }

    public void divide(double x, double y) {
        //vérification si division par zero
        if (y == 0) {
            JFrame jFrame = new JFrame();
            JOptionPane.showMessageDialog(jFrame, "ERROR: impossible calculation");
            fenetre.getResultField().setText("");
        } else {
            setResult(x / y);
        }
    }

    public void modulo(double x, double y) {
        setResult(x % y);
    }

    public void squareRoot(double x) {
        setResult(Math.sqrt(x));
    }

    public void square(double x) {
        setResult(x *= x);
    }

    public void oneByX(double x) {
        setResult(1 / x);
    }

    public void factorial(double x) {
        long fact = 1;
        for (int i = 2; i <= x; i++) {
            fact = fact * i;
        }
        fenetre.getResultField().setText(Long.toString(fact));
    }

    public void sinus(double x) {
        setResult(Math.sin(x * Math.PI / 180));
    }

    public void cosinus(double x) {
        setResult(Math.cos(x * Math.PI / 180));
    }

    public void tangent(double x) {
        setResult(Math.tan(x * Math.PI / 180));
    }

    //affichage du résultat sur la fenetre principale
    public void setResult(double result) {
        fenetre.getResultField().setText(Double.toString(result));
        fenetre.getOperationField().setText("");
    }
    //amélioration possible: séparer complètement les listeners/calculs/affichage
}
