/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.tse.calculator;

import java.awt.event.*;
import javax.swing.ImageIcon;

/**
 *
 * @author Charles Bacchis
 */
//la classe implements ActionListener afin d'executer les évènements automatiquement
public class ButtonListener implements ActionListener {

    private Calculator fenetre; //fenetre principale pour accéder aux objets
    private Calculations calculationwindow; //déclaration de ma classes de calcul

    public ButtonListener(Calculator fenetre) {
        super();
        this.fenetre = fenetre;
        calculationwindow = new Calculations(fenetre); //instanciation de ma classe calcul
    }

    public void actionPerformed(ActionEvent e) {
        /*Pour chaque action effectuée on vérifie quel boutton est associé à
        l'action et on exécture les commandes correspondantes. Je suis conscient
        qu'une cascade de if else n'est pas très beau ou optimisé mais je n'ai
        malheureusement pas trouvé d'autres moyen. un switch case ne peut pas
        comparer 2 bouttons et il n'est pas possible de récupérer le texte du
        boutton à partir de getSource.*/
        
        /*exemple: if (e.getSource().equals(fenetre.getZeroButton())) {
        ici on compare si la source de l'évènement correspond à un certain
        boutton*/
        if (e.getSource().equals(fenetre.getZeroButton())) {
            fenetre.getResultField().setText(fenetre.getResultField().getText() + "0");
        } else if (e.getSource().equals(fenetre.getOneButton())) {
            fenetre.getResultField().setText(fenetre.getResultField().getText() + "1");
        } else if (e.getSource().equals(fenetre.getTwoButton())) {
            fenetre.getResultField().setText(fenetre.getResultField().getText() + "2");
        } else if (e.getSource().equals(fenetre.getThreeButton())) {
            fenetre.getResultField().setText(fenetre.getResultField().getText() + "3");
        } else if (e.getSource().equals(fenetre.getFourButton())) {
            fenetre.getResultField().setText(fenetre.getResultField().getText() + "4");
        } else if (e.getSource().equals(fenetre.getFiveButton())) {
            fenetre.getResultField().setText(fenetre.getResultField().getText() + "5");
        } else if (e.getSource().equals(fenetre.getSixButton())) {
            fenetre.getResultField().setText(fenetre.getResultField().getText() + "6");
        } else if (e.getSource().equals(fenetre.getSevenButton())) {
            fenetre.getResultField().setText(fenetre.getResultField().getText() + "7");
        } else if (e.getSource().equals(fenetre.getEightButton())) {
            fenetre.getResultField().setText(fenetre.getResultField().getText() + "8");
        } else if (e.getSource().equals(fenetre.getNineButton())) {
            fenetre.getResultField().setText(fenetre.getResultField().getText() + "9");
        } else if (e.getSource().equals(fenetre.getEraseButton())) {
            String value = fenetre.getResultField().getText();
            int length = value.length();
            if (length > 0) {
                /*On utilise un string builder pour supprimer le dernier charactère
                à droite*/
                StringBuilder builder = new StringBuilder(value);
                builder.deleteCharAt(length - 1);
                fenetre.getResultField().setText(builder.toString());
            }
            /*il est important de remettre isdecimal sur false après chaque
            opération pour pouvoir le réutiliser*/
        } else if (e.getSource().equals(fenetre.getcButton())) {
            fenetre.getResultField().setText("");
            fenetre.getOperationField().setText("");
            fenetre.setIsDecimal(false);
        } else if (e.getSource().equals(fenetre.getCeButton())) {
            fenetre.getResultField().setText("");
            fenetre.setIsDecimal(false);
        } else if (e.getSource().equals(fenetre.getPlusButton())) {
            fenetre.setSign("+");
            fenetre.setIsDecimal(false);
            if (fenetre.getResultField().getText().length() > 0) {
                storeOperationValue(fenetre.getSign());
            }
        } else if (e.getSource().equals(fenetre.getMinusButton())) {
            fenetre.setSign("-");
            fenetre.setIsDecimal(false);
            if (fenetre.getResultField().getText().length() > 0) {
                storeOperationValue(fenetre.getSign());
            }
        } else if (e.getSource().equals(fenetre.getMultiplicationButton())) {
            fenetre.setSign("×");
            fenetre.setIsDecimal(false);
            if (fenetre.getResultField().getText().length() > 0) {
                storeOperationValue(fenetre.getSign());
            }
        } else if (e.getSource().equals(fenetre.getDivisionButton())) {
            fenetre.setSign("÷");
            fenetre.setIsDecimal(false);
            if (fenetre.getResultField().getText().length() > 0) {
                storeOperationValue(fenetre.getSign());
            }
        } else if (e.getSource().equals(fenetre.getModuloButton())) {
            fenetre.setSign("mod");
            fenetre.setIsDecimal(false);
            if (fenetre.getResultField().getText().length() > 0) {
                storeOperationValue(fenetre.getSign());
            }
        } else if (e.getSource().equals(fenetre.getSquaredRootButton())) {
            fenetre.setSaveValue(Double.parseDouble(fenetre.getResultField().getText()));
            calculationwindow.squareRoot(fenetre.getSaveValue());
        } else if (e.getSource().equals(fenetre.getxSquarredButton())) {
            if (fenetre.getResultField().getText().length() > 0) {
                fenetre.setSaveValue(Double.parseDouble(fenetre.getResultField().getText()));
                calculationwindow.square(fenetre.getSaveValue());
            }
        } else if (e.getSource().equals(fenetre.getOneByXButton())) {
            if (fenetre.getResultField().getText().length() > 0) {
                fenetre.setSaveValue(Double.parseDouble(fenetre.getResultField().getText()));
                calculationwindow.oneByX(fenetre.getSaveValue());
            }
        } else if (e.getSource().equals(fenetre.getPlusByMinusButton())) {
            String value = fenetre.getResultField().getText();
            if (!value.contains("-")) {
                fenetre.getResultField().setText("-" + value);
            } else {
                /*on utilise un try catch dans le cas ou on utiliserai le boutton
                sans avoir entré aucun chiffre*/
                try {
                    fenetre.getResultField().setText(value.split("-")[1]);
                } catch (IndexOutOfBoundsException ex) {
                    fenetre.getResultField().setText("");
                }
            }
        } else if (e.getSource().equals(fenetre.getDotButton())) {
            if (!fenetre.getIsDecimal()) {
                fenetre.getResultField().setText(fenetre.getResultField().getText() + ".");
                fenetre.setIsDecimal(true);
            }
        } else if (e.getSource().equals(fenetre.getFactorialButton())) {
            if (fenetre.getResultField().getText().length() > 0) {
                fenetre.setSaveValue(Double.parseDouble(fenetre.getResultField().getText()));
                calculationwindow.factorial(fenetre.getSaveValue());
            }
        } else if (e.getSource().equals(fenetre.getSinButton())) {
            if (fenetre.getResultField().getText().length() > 0) {
                fenetre.setSaveValue(Double.parseDouble(fenetre.getResultField().getText()));
                calculationwindow.sinus(fenetre.getSaveValue());
            }
        } else if (e.getSource().equals(fenetre.getCosButton())) {
            if (fenetre.getResultField().getText().length() > 0) {
                fenetre.setSaveValue(Double.parseDouble(fenetre.getResultField().getText()));
                calculationwindow.cosinus(fenetre.getSaveValue());
            }
        } else if (e.getSource().equals(fenetre.getTanButton())) {
            if (fenetre.getResultField().getText().length() > 0) {
                fenetre.setSaveValue(Double.parseDouble(fenetre.getResultField().getText()));
                calculationwindow.tangent(fenetre.getSaveValue());
            }
        } else if (e.getSource().equals(fenetre.getEqualButton())) {
            if (fenetre.getResultField().getText().length() > 0) {
                double y = Double.parseDouble(fenetre.getResultField().getText());
                /*Lors de l'appui sur la touche =, on se sert du signe enregistré
                et d'un switch case pour réaliser le calcul approprié en utilisant
                la classe Calculations à travers la variable calculationwindow*/
                switch (fenetre.getSign()) {
                    case "+":
                        calculationwindow.add(fenetre.getSaveValue(), y);
                        break;
                    case "-":
                        calculationwindow.substract(fenetre.getSaveValue(), y);
                        break;
                    case "×":
                        calculationwindow.multiply(fenetre.getSaveValue(), y);
                        break;
                    case "÷":
                        calculationwindow.divide(fenetre.getSaveValue(), y);
                        break;
                    case "mod":
                        calculationwindow.modulo(fenetre.getSaveValue(), y);
                        break;
                }
            }
        } else if (e.getSource().equals(fenetre.getStandardMode())) {
            //mode standard: on fait disparaitre les fonctions scientifiques
            fenetre.getSinButton().setVisible(false);
            fenetre.getCosButton().setVisible(false);
            fenetre.getTanButton().setVisible(false);
            fenetre.getFactorialButton().setVisible(false);
        } else if (e.getSource().equals(fenetre.getScientificMode())) {
            //mode scientifique: on fait apparaitre les fonctions scientifiques
            fenetre.getSinButton().setVisible(true);
            fenetre.getCosButton().setVisible(true);
            fenetre.getTanButton().setVisible(true);
            fenetre.getFactorialButton().setVisible(true);
        }
    }

    public void windowActivated(WindowEvent evt) {
        //mise en place d'une icone de programme
        ImageIcon icon = new ImageIcon("src/resources/caltos.png");
        fenetre.setIconImage(icon.getImage());
    }

    private void storeOperationValue(String sign) {
        //sauvegarde de la première partie de l'opération
        String value = fenetre.getResultField().getText();
        fenetre.setSaveValue(Double.parseDouble(value));
        fenetre.getOperationField().setText(value + " " + sign);
        fenetre.getResultField().setText("");
    }
}
